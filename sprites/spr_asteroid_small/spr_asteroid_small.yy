{
    "id": "c07f89b9-71bc-411b-b0b8-5a8ca7f50198",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_asteroid_small",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5f28e98a-c726-4e4e-9c19-7d0546171412",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c07f89b9-71bc-411b-b0b8-5a8ca7f50198",
            "compositeImage": {
                "id": "5f86deb9-d3f6-4840-8c5e-553bd18e2679",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f28e98a-c726-4e4e-9c19-7d0546171412",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97b29677-3696-41e0-8ffc-53b7c37c4c34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f28e98a-c726-4e4e-9c19-7d0546171412",
                    "LayerId": "34ef57ab-520d-439d-9f5e-4106f9dd4e6a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "34ef57ab-520d-439d-9f5e-4106f9dd4e6a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c07f89b9-71bc-411b-b0b8-5a8ca7f50198",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}