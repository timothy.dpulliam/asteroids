{
    "id": "df88d228-0441-4ee3-b83e-5c7763bb5f39",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 0,
    "bbox_right": 30,
    "bbox_top": 7,
    "bboxmode": 2,
    "colkind": 3,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "21372b6a-a4a8-4e66-aac6-d73e4421c0e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df88d228-0441-4ee3-b83e-5c7763bb5f39",
            "compositeImage": {
                "id": "22eaa762-3725-47cb-8ade-79b24fcf1c33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21372b6a-a4a8-4e66-aac6-d73e4421c0e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28275b60-8cc7-4ac4-84d8-4ac190eebf5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21372b6a-a4a8-4e66-aac6-d73e4421c0e9",
                    "LayerId": "b0820e6e-538a-423d-a911-0c35f3621079"
                }
            ]
        }
    ],
    "gridX": 4,
    "gridY": 4,
    "height": 32,
    "layers": [
        {
            "id": "b0820e6e-538a-423d-a911-0c35f3621079",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "df88d228-0441-4ee3-b83e-5c7763bb5f39",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}