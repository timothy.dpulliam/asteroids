{
    "id": "80046309-4a93-48a9-8d53-908e76eb2529",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_debris",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8c2ecdf0-2ad4-4184-9c20-db6f4aab56c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "80046309-4a93-48a9-8d53-908e76eb2529",
            "compositeImage": {
                "id": "83c5fc2f-a95b-40de-9a00-09b93b522d76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c2ecdf0-2ad4-4184-9c20-db6f4aab56c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78b7a485-3f4b-4852-adad-9575167c6bc7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c2ecdf0-2ad4-4184-9c20-db6f4aab56c2",
                    "LayerId": "ff2887db-133a-4799-956f-ee32e3eeab35"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "ff2887db-133a-4799-956f-ee32e3eeab35",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "80046309-4a93-48a9-8d53-908e76eb2529",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1,
    "xorig": 0,
    "yorig": 0
}