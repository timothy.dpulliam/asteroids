{
    "id": "2ddf7ea3-3e4f-4fe9-a1f3-3b324d609bd7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1,
    "bbox_left": 0,
    "bbox_right": 1,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e25e30a2-d967-486d-9e0e-da4b43465545",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ddf7ea3-3e4f-4fe9-a1f3-3b324d609bd7",
            "compositeImage": {
                "id": "12e65abe-056b-4790-89c5-35c0517be1e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e25e30a2-d967-486d-9e0e-da4b43465545",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03cf0a68-d8d6-43ee-903b-cec1d1786f8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e25e30a2-d967-486d-9e0e-da4b43465545",
                    "LayerId": "c9e8cf34-d707-427a-a01f-7fa5c7ccc9c8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2,
    "layers": [
        {
            "id": "c9e8cf34-d707-427a-a01f-7fa5c7ccc9c8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2ddf7ea3-3e4f-4fe9-a1f3-3b324d609bd7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2,
    "xorig": 0,
    "yorig": 0
}