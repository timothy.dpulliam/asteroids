{
    "id": "95ade982-c742-4701-b7e4-71806ba4ba44",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_asteroid_med",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 29,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fca02e13-6900-48a7-8215-5e1a58107448",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95ade982-c742-4701-b7e4-71806ba4ba44",
            "compositeImage": {
                "id": "be807bc7-1b90-41eb-8d4c-803dc61a20d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fca02e13-6900-48a7-8215-5e1a58107448",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "712489fe-ef02-4133-aed4-9a2369cab3c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fca02e13-6900-48a7-8215-5e1a58107448",
                    "LayerId": "435f6772-7267-48aa-af5d-72c680260481"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "435f6772-7267-48aa-af5d-72c680260481",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "95ade982-c742-4701-b7e4-71806ba4ba44",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}