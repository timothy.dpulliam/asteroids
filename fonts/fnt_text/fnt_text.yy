{
    "id": "5b4352d6-31ca-4c8c-b08e-d66789fd3276",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_text",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Consolas",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "155dcbad-8adf-43c2-9eb4-3ab22df5ac86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "a6fe345f-2b03-4aa7-8acc-f1bc2ec1d0f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 22,
                "offset": 4,
                "shift": 10,
                "w": 3,
                "x": 207,
                "y": 50
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "350707ef-adc0-42a0-991e-c1c28fc524af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 199,
                "y": 50
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "3da75724-f26b-463f-b4e3-75f6bc903aba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 187,
                "y": 50
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "7aa776ec-4111-4ba7-ac3d-035e0624d7bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 176,
                "y": 50
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "fd7ad9d9-f3a9-4d90-9bc4-91bdc0b16c99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 164,
                "y": 50
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "d881672e-7c42-42ac-a661-5e090559170e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 151,
                "y": 50
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "9ae4b97b-4a7c-4a09-ac3e-e34925b9aef7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 22,
                "offset": 4,
                "shift": 10,
                "w": 3,
                "x": 146,
                "y": 50
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "62b66ead-df9f-4ba0-bdaa-83c4c08dd5b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 22,
                "offset": 3,
                "shift": 10,
                "w": 5,
                "x": 139,
                "y": 50
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "51b089ab-961c-4be9-a971-66806f9e8ca7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 131,
                "y": 50
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "3b0b09d0-137b-4dd5-b171-7933d0558f41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 212,
                "y": 50
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "2d56cb40-f012-4da1-86ef-adf5f54c5fd9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 120,
                "y": 50
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "a17a933f-1ab9-4f67-8c47-0013127c9385",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 5,
                "x": 103,
                "y": 50
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "497f6978-835b-46c8-aa2d-30981c2b3515",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 95,
                "y": 50
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "64b6e781-93bc-491f-935b-a7f331d321aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 22,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 89,
                "y": 50
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "72ef04b2-f115-4290-9749-cb76df2a5417",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 79,
                "y": 50
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "717dd794-9c76-43ec-95fd-b30970134df8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 67,
                "y": 50
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "1c9769cf-2ea1-46ab-aadd-6ae9131f701b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 57,
                "y": 50
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "e459e3ad-1337-40d2-a519-7e5e5a2bb57e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 47,
                "y": 50
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "741b3e18-cf87-48d2-91be-2f8b08eb18d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 37,
                "y": 50
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "adef7185-9b46-4fcf-8a9a-59a68dc5df01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 25,
                "y": 50
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "2972ec50-ece8-4212-b622-0d24c7fab15a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 110,
                "y": 50
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "9971705d-b2b9-4640-834f-08821fb8cb5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 222,
                "y": 50
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "f4aa7977-f596-4afe-a6ba-d91d2832030b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 233,
                "y": 50
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "4a721247-4bd6-4edd-851b-d7fe683e5a0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 243,
                "y": 50
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "44c79b76-851e-4e6b-a4cc-843c5405f1b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 199,
                "y": 74
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "b86f6f35-78be-430c-8f4b-f9c665b2c957",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 22,
                "offset": 4,
                "shift": 10,
                "w": 3,
                "x": 194,
                "y": 74
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "d9545106-73c1-42f3-8d22-4851d4fb1f32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 5,
                "x": 187,
                "y": 74
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "5ff15cea-c0c5-4f15-b068-ffa047575a9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 177,
                "y": 74
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "a795d7fb-811f-4c56-a905-9d8199153913",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 167,
                "y": 74
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "ec4fcf1c-18ef-43fd-9d1f-64d54a5755bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 8,
                "x": 157,
                "y": 74
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "c50cd5db-9dee-4379-838d-40d11265f995",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 22,
                "offset": 3,
                "shift": 10,
                "w": 6,
                "x": 149,
                "y": 74
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "f487c93c-3472-435d-bd64-659f660993ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 137,
                "y": 74
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "f067e57f-3879-4512-898e-ccda72316152",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 125,
                "y": 74
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "aa2087ab-1516-4414-884b-5dd2020779be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 114,
                "y": 74
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "064bb919-9d49-440c-be0e-f86736acbbea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 104,
                "y": 74
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "65bc0046-be9d-4baf-8d71-779353a2e99e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 93,
                "y": 74
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "319cb963-2768-4f7f-9339-463c3a25823d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 84,
                "y": 74
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "9b5dd77b-711f-4499-b9f3-6dadec85b60d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 75,
                "y": 74
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "886d23e8-2ce1-49c4-8b47-11cbf4fb6f02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 63,
                "y": 74
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "d264adf0-f671-4c1f-8ef9-975b31c6a852",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 52,
                "y": 74
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "522da023-a515-445b-96e4-c3aa837b8baa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 42,
                "y": 74
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "32fafd49-9c96-473d-8846-ae76c3003179",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 34,
                "y": 74
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "9e38e97b-de91-4c23-95c0-59de83134c40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 23,
                "y": 74
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "dd0a3b3a-e397-4cf4-a276-b0bcccb2c07b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 14,
                "y": 74
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "2eff0d82-c69e-4edf-a4be-a644601fea7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 2,
                "y": 74
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "fe45eac9-5d9d-431f-8c95-db7ed831c649",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 14,
                "y": 50
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "79417de7-60dd-4afe-be67-9e26b85b762b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 2,
                "y": 50
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "170f646c-4954-4407-86c2-4ca0c1d39cb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 236,
                "y": 26
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "a1cca2a9-ca2e-492a-b74f-f20a620c02b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 235,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "2c9b6cb1-4ad4-43bf-a5ba-974a9810489d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 217,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "49693330-e853-4fe2-9179-e9e6f1a3ee14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 207,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "5eb40827-3663-4893-8a18-9193357c949a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 196,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "124f101e-0648-4402-853e-2ed56b4b8a48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 185,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "e81aefbe-87d4-4ac8-b5c2-49c889da3c5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 173,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "58578a2d-8df2-464b-a029-9eec502afe5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 161,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "f065e614-53bc-445e-9ec9-289693508f19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 149,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "7913a0f2-eecd-4f3e-93a7-36274226bd62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 137,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "45112f78-089f-4ad5-9d09-dde585b3c77f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 127,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "e3314801-5e9a-420d-9d30-f9cbffc7b481",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 22,
                "offset": 3,
                "shift": 10,
                "w": 5,
                "x": 228,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "6a9634a7-9817-4d4a-93cd-67e16d3f71cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 116,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "b9313344-34d7-4ff3-95a0-ca6902511ad7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 5,
                "x": 97,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "20b8412c-4ff5-48dc-bf61-8df17d674bb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 86,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "0f2ee6ec-4cff-45e9-a828-465a25e63fd6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 74,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "b2a76dff-162a-4c1a-916a-5bbffea316ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 6,
                "x": 66,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "30850316-c7c6-4381-99df-2079654af06f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 56,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "4a9206fc-4673-49c4-93c6-29f7f0cae9f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 45,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "34fef20c-5de2-4658-ab59-34082a42f55e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 35,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "300c4f55-df2f-4753-9c10-63df81126358",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 25,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "0d094acf-e129-4215-b8aa-2351202a7314",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 14,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "bdb6b1fd-7a25-4f6a-a6f5-c508a4c9e2c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 104,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "4a7f7e62-593f-4648-956f-b2630d784355",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 2,
                "y": 26
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "177a1fe8-e618-4539-9c60-1f82e090edd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 113,
                "y": 26
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "1e30a5ed-ae26-4599-a7fa-723c94b4b7db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 13,
                "y": 26
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "bea1ff4a-dc3d-42ed-85a3-2a9b60c25f0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 217,
                "y": 26
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "d7db054b-6738-4c1d-9fd6-713ebcee795c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 206,
                "y": 26
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "ee8c4cdc-a871-4396-98cb-3ef4342ec623",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 196,
                "y": 26
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "0fd24625-9999-463d-a294-af1067af9097",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 185,
                "y": 26
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "a4413f35-782e-4251-91ec-96b31925241b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 175,
                "y": 26
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "796df6fd-3e19-4f67-b29d-e9aaf7a1d5d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 164,
                "y": 26
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "f7de90bc-76d9-4843-9d09-4608aac92efd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 153,
                "y": 26
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "bd102544-5b74-47c5-bff6-0f1f63426376",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 143,
                "y": 26
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "f064439a-1cce-400b-b281-f0003e4f11f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 8,
                "x": 133,
                "y": 26
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "47aa7bb1-3ecd-481b-b929-e0e23452896c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 226,
                "y": 26
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "1bb28698-55f2-4321-a8b9-aef563c3f46c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 123,
                "y": 26
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "f3b036b9-a40f-45fd-b1c8-9caaf4c22391",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 103,
                "y": 26
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "4c42831f-ec86-4392-a6f1-81c28cca83b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 91,
                "y": 26
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "4f1c1215-2d21-46df-bfa4-23c873d89203",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 79,
                "y": 26
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "8ed92b85-5634-4590-b747-1a6c791904f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 67,
                "y": 26
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "1a34f236-4848-4fb8-b5dc-7d4e3d30509a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 55,
                "y": 26
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "75014cbb-485e-479a-b1ec-fd41d3f6f622",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 45,
                "y": 26
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "04d91c7a-3c54-47a5-b888-caa13bd4e2e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 36,
                "y": 26
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "c10415bf-4e49-4097-9977-a3f6da48ed21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 22,
                "offset": 4,
                "shift": 10,
                "w": 2,
                "x": 32,
                "y": 26
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "c041561b-a12c-4e00-a46c-6f7e08625a2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 23,
                "y": 26
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "6cf4ee99-6f2e-4b9f-9bce-f35674751044",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 209,
                "y": 74
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "9c50b339-0c16-4cdc-85e9-1de93cb9c42c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 22,
                "offset": 4,
                "shift": 18,
                "w": 11,
                "x": 221,
                "y": 74
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 14,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}