{
    "id": "ca507840-5d5c-418d-8224-d7bfa2482ca9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship",
    "eventList": [
        {
            "id": "8188a9cb-0d3e-4808-b14e-7315c4b784e7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ca507840-5d5c-418d-8224-d7bfa2482ca9"
        },
        {
            "id": "875f0dc6-6003-45bc-a92c-774eb59f217d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "04a66998-ca28-4644-850b-2a63a9f2e31c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "ca507840-5d5c-418d-8224-d7bfa2482ca9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "df88d228-0441-4ee3-b83e-5c7763bb5f39",
    "visible": true
}