/// @description Insert description here
// You can write your code in this editor

// keboard_check() With this function you can check to see if a key
// is held down or not.
if(keyboard_check(vk_left)){
	image_angle += 5;
}

if(keyboard_check(vk_right)){
	image_angle -= 5;
}

if(keyboard_check(vk_up)){
	motion_add(image_angle, 0.05);
    // x += 5*dcos(image_angle);
	// y -= 5*dsin(image_angle);
}

if(keyboard_check(vk_down)){
	motion_add(image_angle, -0.05);
	// x -= 5*dcos(image_angle);
	// y += 5*dsin(image_angle);
}

move_wrap(true, true, sprite_width/2)

// With this function you can check to see if a key has been pressed or not.
if(keyboard_check_pressed(vk_space)){
	var bullet_id = instance_create_layer(x, y, "Instances", obj_bullet);
	bullet_id.direction = image_angle
	audio_play_sound(snd_bullet, 1, false);
}