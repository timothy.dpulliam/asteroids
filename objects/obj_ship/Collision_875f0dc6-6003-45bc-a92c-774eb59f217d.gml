/// @description Insert description here
// You can write your code in this editor

global._lives -= 1;
instance_destroy();
audio_play_sound(snd_explode, 1, false);

with(obj_game){
	alarm[1] = room_speed;
}

repeat(10){
	instance_create_layer(x, y, "Instances", obj_debris);
}