/// @description Insert description here
// You can write your code in this editor

global._score += 10;
audio_play_sound(snd_explode, 1, false);
instance_destroy();

// Alternatively, you could use other.method()
with(other){
	instance_destroy();
	
	if (sprite_index == spr_asteroid_big){
		repeat(2){
			var new_asteroid = instance_create_layer(x, y, "Instances", obj_asteroid);
			new_asteroid.sprite_index = spr_asteroid_med;
		}
	} else if (sprite_index == spr_asteroid_med){
		repeat(2){
			var new_asteroid = instance_create_layer(x, y, "Instances", obj_asteroid);
			new_asteroid.sprite_index = spr_asteroid_small;
		}
	}
	
	repeat(10){
		instance_create_layer(x, y, "Instances", obj_debris);
	}
}