{
    "id": "4304ac36-5c18-4511-ac63-e01ad585fb40",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bullet",
    "eventList": [
        {
            "id": "7e2d6dc7-ad0b-42fd-9e30-8819c937afa4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4304ac36-5c18-4511-ac63-e01ad585fb40"
        },
        {
            "id": "4bd3e9fd-1b4f-4442-b225-9c01931ed9fe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "04a66998-ca28-4644-850b-2a63a9f2e31c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "4304ac36-5c18-4511-ac63-e01ad585fb40"
        },
        {
            "id": "20db3ae1-e071-4dc3-96c2-e9e4f13242dc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "4304ac36-5c18-4511-ac63-e01ad585fb40"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2ddf7ea3-3e4f-4fe9-a1f3-3b324d609bd7",
    "visible": true
}