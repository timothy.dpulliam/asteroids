{
    "id": "04a66998-ca28-4644-850b-2a63a9f2e31c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_asteroid",
    "eventList": [
        {
            "id": "41649ace-0ae2-410e-b69d-eb9d4a191661",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "04a66998-ca28-4644-850b-2a63a9f2e31c"
        },
        {
            "id": "bcbffa56-1d1b-4112-b330-7100c3821e83",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "04a66998-ca28-4644-850b-2a63a9f2e31c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c07f89b9-71bc-411b-b0b8-5a8ca7f50198",
    "visible": true
}