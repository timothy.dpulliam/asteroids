/// @description Insert description here
// You can write your code in this editor

if(keyboard_check_pressed(vk_enter)){
	switch(room){
		case rm_start:
		    room_goto(rm_level_transition);
			break;
		
		case rm_win:
		case rm_gameover:
		    game_restart();
			break;
	}
}

if(room == rm_game){
	if(global._score >= 1000){
		global._score = 0;
		global._level += 1;
		room_goto(rm_level_transition);
		// audio_play_sound(snd_win, 1, false);
	}

	if(global._lives <= 0){
		room_goto(rm_gameover);
		// audio_play_sound(snd_gameover, 1, false);
	}
}