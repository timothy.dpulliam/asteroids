/// @description Insert description here
// You can write your code in this editor

// If we are not in the game room, we do not want the alarm 
// to go off.
if(room != rm_game){
	exit;
}

if(choose(0, 1) == 0){
	// Spawn asteroid on side of screen
	var xx = choose(0, room_width);
	var yy = irandom_range(0, room_height);
} else {
	// Spawn asteroid at top of screen
	var xx = irandom_range(0, room_width);
	var yy = choose(0, room_height);
}

instance_create_layer(xx, yy, "Instances", obj_asteroid);

// Every four seconds an asteroid appears
// Every 4 seconds, the code in this alarm is executed.
alarm[0] = 4*room_speed;