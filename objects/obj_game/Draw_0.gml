/// @description Insert description here
// You can write your code in this editor

//draw_text(20, 20, "SCORE: "+string(global._score));
//draw_text(20, 40, "LIVES: "+string(global._lives));


switch(room){
	case rm_start:
	    // fa_center, font align center
		// fa_center, fa_left, fa_right
	    draw_set_halign(fa_center);
		draw_text_transformed_color(
			room_width/2, 100, "ASTEROIDS",
			3, 3, 0, c_yellow, c_yellow, c_red, c_red, 1
			);
		draw_text(
			room_width/2, 200,
@"Score 1,000 points to get to the next level!

UP: move forward
DOWN: move backward
LEFT/RIGHT: change direction
SPACE: shoot

>> PRESS ENTER TO START <<
"
		);
		draw_set_halign(fa_left);
		break;
	
	case rm_level_transition:
		draw_set_halign(fa_center);
		draw_text_transformed_color(
			room_width/2, 100, "LEVEL: " + string(global._level),
			3, 3, 0, c_white, c_white, c_white, c_white, 1
			);
		draw_set_halign(fa_left);
		break;

	case rm_game:
		draw_text(20, 20, "SCORE: " + string(global._score));
		draw_text(20, 40, "LIVES: " + string(global._lives));
		break;

	case rm_win:
		draw_set_halign(fa_center);
		draw_text_transformed_color(
			room_width/2, 100, "YOU WIN",
			3, 3, 0, c_lime, c_lime, c_lime, c_lime, 1
			);
		draw_text(
			room_width/2, 300,
			"PRESS ENTER TO RESTART"
		);
		draw_set_halign(fa_left);
		break;
		
	case rm_gameover:
		draw_set_halign(fa_center);
		draw_text_transformed_color(
			room_width/2, 100, "GAME OVER",
			3, 3, 0, c_red, c_red, c_red, c_red, 1
			);
		draw_text(
			room_width/2, 250,
			"YOU MADE IT TO LEVEL " + string(global._level)
		);
		draw_text(
			room_width/2, 300,
			"PRESS ENTER TO RESTART"
		);
		draw_set_halign(fa_left);
		break;
}