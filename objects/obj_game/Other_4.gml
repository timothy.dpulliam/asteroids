/// @description Insert description here
// You can write your code in this editor


if(room == rm_start){
	if(audio_is_playing(msc_song)){
		audio_stop_sound(msc_song);
	}
	audio_play_sound(msc_song, 2, true);
}

if(room == rm_game){
	
	if(!audio_is_playing(msc_song)){
		audio_play_sound(msc_song, 2, true);
	}

	
	// every level, more asteroids start in the level
	repeat(6 + global._level){
		var xx = choose(
			irandom_range(0, room_width*0.3),
			irandom_range(room_width*0.7, room_width)
			);
		var yy = choose(
			irandom_range(0, room_height*0.3),
			irandom_range(room_height*0.7, room_height)
			);
		instance_create_layer(xx, yy, "Instances", obj_asteroid);
	}
	
	// Alarm goes off after 60 steps (1 second)
	// The code in Alarm 0 is executed every second
	alarm[0] = 60;
}

